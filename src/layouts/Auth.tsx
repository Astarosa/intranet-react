import { Switch, Route, Redirect } from "react-router-dom";

//components
// navbar

// views
import Login from "../views/Login";

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const Auth = () => {
  return (
    <>
      <main>
        <section className="relative w-full h-full py-40 min-h-screen">
          <div
            className="absolute top-0 w-full h-full bg-gray-900 bg-no-repeat bg-full"
            // style={{
            //   backgroundImage:
            //     "url(" + require("assets/img/signup-background") + ")",
            // }}
          ></div>
          <Switch>
            <Route path="/auth/login" exact component={Login} />
            <Redirect from="/auth" to="/auth/login" />
          </Switch>
        </section>
      </main>
    </>
  );
};

export default Auth;
