import { useState, useEffect, useContext, createContext } from "react";
import { auth } from "../services/firebase";

const AuthContext = createContext(null);

export const useAuth = (): JSX.Element => useContext(AuthContext);

export const AuthProvider: React.FC = ({ children }) => {
  const [currentUser, setCurrentUser] = useState(null);
  const [isAuthenticating, setIsAuthenticating] = useState(true);

  const signInWithEmailAndPassword = (email: unknown, password: unknown) => {
    return auth.signInWithEmailAndPassword(email, password);
  };

  const logout = () => {
    return auth.signOut().then(() => {
      setCurrentUser(null);
    });
  };

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((user: any) => {
      setCurrentUser(user);
      setIsAuthenticating(false);
    });

    return () => unsubscribe();
  }, []);

  const values = {
    currentUser,
    isAuthenticating,
    signInWithEmailAndPassword,
    logout,
  };

  return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>;
};
