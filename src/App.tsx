import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

// layouts

import Admin from "./layouts/Dashboard";
import Auth from "./layouts/Auth";

const App = (): JSX.Element => {
  return (
    <>
      <Router>
        <Switch>
          <Route path="/admin" component={Admin} />
          <Route path="/auth" component={Auth} />
        </Switch>
      </Router>
    </>
  );
};

export default App;
